<?php

return [
    'id' => 'nds',
    'request' => [
        'cookieValidationKey' => 'qwwdweedwdeedw',
    ],
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName'=> false,
    ],
];