<?php
namespace app\v1\controllers;

use yii\rest\Controller;

class EntryPointController extends Controller
{
    public function actionIndex()
    {
        return ['nds'];
    }
}